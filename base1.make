;Drupal Core
api = 2
core = 7.x
projects[drupal][type] = core

; Administration
projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][subdir] = contrib

;projects[masquerade][subdir] = contrib

;projects[backup_migrate_files][subdir] = contrib

projects[module_filter][subdir] = contrib

;projects[coffee][version] = "1.0"
;projects[coffee][subdir] = contrib

;projects[admin_views][version] = "1.3"
;projects[admin_views][subdir] = contrib



; Design / Theming

;projects[zurb-foundation][version] = "5.x-dev"

;projects[fontyourface][subdir] = "contrib"

;projects[block_class][version] = "1.3"
;projects[block_class][subdir] = contrib

;projects[ds][version] = "2.6"
;projects[ds][subdir] = contrib

;projects[fences][subdir] = contrib

;projects[field_formatter_settings][subdir] = contrib

;projects[menu_attributes][subdir] = contrib

;projects[panels_extra_styles][subdir] = contrib

;projects[styleguide][subdir] = contrib

;projects[field_group][version] = "1.4"
;projects[field_group][subdir] = "contrib"


;media & files
;projects[stage_file_proxy][subdir] = contrib
;projects[views_pdf][subdir] = contrib

;development tools
projects[devel][version] = "1.5"
projects[devel][subdir] = contrib

;projects[devel_themer][subdir] = contrib

;projects[simplehtmldom][subdir] = contrib

;projects[coder][subdir] = contrib

;projects[diff][version] = "3.2"
;projects[diff][subdir] = contrib

;projects[media][version] = "2.x-dev"
;projects[media][subdir] = contrib

;projects[media_youtube][version] = "2.x-dev"
;projects[media_youtube][subdir] = contrib

;projects[zurb_clearing][subdir] = contrib



;deployment & versioning

;projects[shield][subdir] = contrib

;projects[features][version] = "2.0"
;projects[features][subdir] = contrib
;projects[features][patch][] = "http://drupal.org/files/766264-25-alter-override.patch"

;projects[strongarm][subdir] = contrib

;projects[habitat][version] = "1.x-dev"
;projects[habitat][subdir] = contrib

; Email
;projects[mailsystem][subdir] = contrib

;projects[mimemail][subdir] = contrib

;projects[phpmailer][subdir] = contrib

;projects[mailchimp][subdir] = contrib

;projects[reroute_email][subdir] = contrib



;UI/Usability
;projects[breadcrumbs_by_path] = "1.0-alpha11"
;projects[breadcrumbs_by_path][subdir] = "contrib"

;projects[inline_entity_form][subdir] = "contrib"

;projects[chosen][subdir] = "contrib"
;projects[chosen][version] = "2.0-beta4"

;projects[plup][version] = "1.x-dev"
;projects[plup][subdir] = contrib

;projects[menu_block][version] = "2.3"
;projects[menu_block][subdir] = contrib

;projects[menu_position][version] = "1.1"
;projects[menu_position][subdir] = contrib

;projects[prepopulate][subdir] = contrib

;projects[content_menu][subdir] = contrib

;projects[navbar][subdir] = contrib

;projects[ccl][subdir] = contrib




; Fields
;projects[insert][subdir] = contrib

;projects[entityreference][version] = "1.1"
;projects[entityreference][subdir] = contrib

;projects[addressfield][subdir] = contrib

;projects[countries][subdir] = contrib

;projects[date][version] = "2.7"
;projects[date][subdir] = contrib

;projects[email][subdir] = contrib

;projects[link][version] = "1.2"
;projects[link][subdir] = contrib

;projects[linkit][version] = "2.6"
;projects[linkit][subdir] = contrib

;projects[field_collection][subdir] = contrib

;projects[video_embed_field][subdir] = contrib

;projects[logintoboggan][subdir] = "contrib"
;projects[logintoboggan][version] = "1.3"

;projects[file_entity][version] = "2.x-dev"
;projects[file_entity][subdir] = "contrib"


;User/Permissions
;projects[fpa][subdir] = contrib

;projects[login_destination][subdir] = contrib

;projects[email_registration][subdir] = contrib

;projects[realname][subdir] = contrib


; Helper/Utility Modules
;projects[entity][version] = "1.5"
;projects[entity][subdir] = contrib
;projects[entity][patch][] = "http://drupal.org/files/2003826-16-check_path_index_uri.patch"

;projects[libraries][version] = "2.2"
;projects[libraries][subdir] = contrib

;projects[jquery_update][version] = "2.4"
;projects[jquery_update][subdir] = contrib

;projects[uuid][version] = "1.x"
;projects[uuid][subdir] = contrib

;projects[advanced_help][subdir] = contrib

;projects[token][version] = "1.5"
;projects[token][subdir] = contrib

;projects[pathauto][version] = "1.2"
;projects[pathauto][subdir] = contrib

;projects[subpathauto][subdir] = contrib

;projects[ctools][version] = "1.4"
;projects[ctools][subdir] = contrib

;projects[entitycache][version] = "1.2"
;projects[entitycache][subdir] = contrib

;projects[references_dialog][subdir] = contrib


; spam
;projects[honeypot][version] = "1.17"
;projects[honeypot][subdir] = contrib


Libraries
;libraries[jquery.cycle][download][type] = "git"
;libraries[jquery.cycle][download][url] = "https://github.com/malsup/cycle.git"
;libraries[jquery.cycle][directory_name] = "jquery.cycle"
;libraries[jquery.cycle][type] = "library"

;libraries[mailchimp][download][type] = "file"
;libraries[mailchimp][download][url] = "http://apidocs.mailchimp.com/api/downloads/mailchimp-api-class.zip"
;libraries[mailchimp][directory_name] = "mailchimp"
;libraries[mailchimp][type] = "library"

;libraries[ckeditor][download][type] = get
;libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.0.1/ckeditor_4.0.1_standard.tar.gz
;libraries[ckeditor][destination] = libraries

;libraries[phpmailer][download][type] = get
;libraries[phpmailer][download][url] = http://phpmailer.apache-extras.org.codespot.com/files/PHPMailer_5.2.2.tgz
;libraries[phpmailer][destination] = libraries

;libraries[plupload][download][type] = "get"
;libraries[plupload][download][url] = "https://github.com/moxiecode/plupload/archive/v2.1.1.zip"
;libraries[plupload][directory_name] = "plupload"
;libraries[plupload][type] = "library"

;libraries[flexslider][download][type] = "get"
;libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/archive/version/2.1.zip"

; SEO
;projects[seo_checklist][subdir] = contrib

;projects[checklistapi][subdir] = contrib

;projects[site_map][subdir] = contrib

;projects[site_verify][subdir] = contrib

;projects[page_title][subdir] = contrib

;projects[xmlsitemap][version] = "2.0"
;projects[xmlsitemap][subdir] = contrib

;projects[google_analytics][subdir] = contrib

;projects[redirect][subdir] = "contrib"
;projects[redirect][version] = "1.0-rc1"

;projects[globalredirect][version] = "1.5"
;projects[globalredirect][subdir] = "contrib"

;projects[metatag][subdir] = contrib


; Site Building
;projects[nodequeue][subdir] = contrib

;projects[image_focus][subdir] = contrib

;projects[ckeditor][subdir] = contrib

;projects[context][subdir] = "contrib"

;projects[context_omega][subdir] = "contrib"

;projects[print][subdir] = "contrib"



; Views
;projects[views][version] = "3.8"
;projects[views][subdir] = contrib
;projects[views][patch][] = "http://drupal.org/files/views-fix-destination-link-for-ajax-1036962-29.patch"

;projects[better_exposed_filters][subdir] = contrib

;projects[views_bulk_operations][version] = "3.2"
;projects[views_bulk_operations][subdir] = "contrib"

;projects[views_data_export][subdir] = "contrib"

;projects[flexslider][version] = "2.0-alpha1"
;projects[flexslider][subdir] = "contrib"

;projects[flexslider_views_slideshow][version] = "2.x-dev"
;projects[flexslider_views_slideshow][subdir] = "contrib"

;project[rules][subdir] = "rules"







